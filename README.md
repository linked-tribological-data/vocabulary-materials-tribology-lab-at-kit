The purpose of this controlled vocabulary of terms is to describe the metadata fields needed when collecting data in an experimental materials tribology lab at the Karlsruhe Institute of Technology (KIT).

When used in conjunction with an Electronic Lab Notebook, the vocabulary provides the framework for creating a FAIR (findable, accessible, interoperable and reusable) data. An example term can be found here: https://purls.helmholtz-metadaten.de/vp/kitmtxx-41732562-vp/bea_cur_xxx-893d00f15026414883b0f5d2c9298daf-vp

**_This is a live digital object, which is regularly updated and augmented. Please check the referenced for more information._**

--------
**Scope:** Materials Science, Mechanical Engineering, Tribology, Information Science

**FAIR-ness evaluation of resource:** Based on the following publication, this vocabulary satisfies all FAIR criteria (https://doi.org/10.1371/journal.pcbi.1009041)

**Size of Vocabulary:** 1067 terms, hierarchically organized (9790 semantic triples)

-------
**Description of Files:** 

- **Individual_Terms**: Folder containing individual files with Turtle serializations of each term
- **Individual_Terms.zip:** Archive file containing Turtle serializations of each term separated in individual files
- **LICENSE:** File containing the CC-BY 4.0 License
- **README.md:** Cover information of the digital object
- **SKOS_Vocabulary_Materials_Tribology_Lab_at_KIT.json:** JSON-LD file containing the entire SKOS vocabulary
- **SKOS_Vocabulary_Materials_Tribology_Lab_at_KIT.ttl:** Turtle file containing the entire SKOS vocabulary
- **Vocabulary_Metadata.json:** Basic metadata of the vocabulary


**Related Resources** 
- https://doi.org/10.5281/zenodo.7709547 - Archive of released version

--------
**Top-Level Terms in Vocabulary:**

 **Processes**
- CAD Modelling
- Commercial Machining
- Cup Grinding
- Data Processing
- Data Processing (Integrated Data)
- Data Processing (Optimol Data)
- Data Processing (SEM Data)
- Data Processing (Sensofar Data)
- Debedding
- Demagnetization
- Electropolishing
- Embedding
- Equipment Calibration
- FE-Analysis
- FFF-Printing
- FIB Sample Preparation
- FIB Stub Detachment
- Grease Seal Renewal
- Grinding
- Hradness Measurement
- Heat Treatment
- Laser Processing
- Light Microscopy
- Metal Sawing
- Object Receival
- Optical Surface Profilometry
- Polishing
- SEM Session
- Silica Gel Renewal
- Specimena Cleaning
- Specimen Storange Handling
- Strucutral Mechanical Optimization
- Tactile Surface Profilometry
- Tensile Test
- Toolpath Generation
- Tribological Experiment
- Weighing

 **Subprocesses**
- SEM or FIB Imaging
- FIB Cutting

 **Publication**
- Data Publication
- Publication

 **Equipment**
- Band Saw
- Cup Grinding Machine
- Demagnetizing Plate
- Desiccator
- Dry Cabinet
- Electropolishing Machine
- FFF-Printer
- Furnace
- Grinding Machine
- Hardness Tester
- Laser Processing System
- Light Microscope
- Optical Surface Profilometer
- Polishing Machine
- Precision Scale
- Scanning Electron Microscope
- Tactile Surface Profilometer
- Tensile Tester
- Tribometer
- Ultrasonic Cleaner
- Wire Saw

 **Specimen**
- Block Specimen
- Coated Specimen

 **Consumable**
- Interfacial Lubricating Medium

 **Software**
- Software

 **Object**
- Sphere Storage Box

 **Data Objects**
- File (Surface Profilometry)
- File (Hardness Measurement)
- File (Data Processing)
- File (Electron Microscopy)
- File (Tribological Experiment)

**Record Links**
- physicallyModifies
- characterizesSpecimen
- employsEquipment
- physicallyResultsIn
- stressesAsBaseBody
- stressesAsCounterBody
- takesDataFromProcess
- usesSoftware
- makesUseOfConsumable
- queriesPositionOfWearTrackFromProcess
- providesAdditionalInformationAbout
- queriesPositionOfFeatureFomProcess
- originatesFromSpecimen
- isModifiedVersionOf
- documentsDescriptionOf
- deposits
- withdraws
- involves

-------
**Version History:**

- **0.3.0:** Improvements of process descriptions and addition of new processes
- **0.2.0:** Added terms, as the vocabulary was used for validation of lab data (https://doi.org/10.5281/zenodo.7923128)
- **0.1.0:** Initial vocabulary

**Contributors:**

- Malte Flachmann, "ORCID": "0000-0002-0802-3480"
- Ilia Bagov, "ORCID": "0000-0002-9094-8959"
- Nikolay Garabedian, "ORCID": "0000-0003-4049-4212"
- Torben Tiezema
- Yulong Li
- Julia Rau
- Ines Blatter, "ORCID": "0000-0002-4499-0355"
- Antje Dollmann
- Michael Seitz, "ORCID": "0000-0002-6750-4719"
- Amelie Schiele,
- Iwiza Tesari
- Christian Greiner, "ORCID": "0000-0001-8079-336X"

-------
**The Vocabulary Cites:**

-  TriboDataFAIR Ontology, https://doi.org/10.5281/zenodo.7716129

**The Vocabulary is Related to:**

-  EM_Glossary, https://codebase.helmholtz.cloud/em_glossary/em_glossary


